export const asifData = {
  mainBanner: {
    title: 'Hello, I am',
    yourName: 'Asif Sheraz',
    jobPositions:
      ' Program Management | Project Execution | Scaled Agile Transformation',
    resumeLink: 'assets/cv/Asif Sheraz.pdf',
    buttonText: 'Print Resume',
    portfolioLink: 'https://gitlab.com',
    certification: [
      {
        certificateLink: '',
        class: ' psm cert-badge',
        badgeLink: 'assets/imgs/pmi.png',
        alt: 'badge',
        title: '',
      },
      {
        certificateLink: '',
        class: 'psm cert-badge',
        badgeLink: 'assets/imgs/axe.jpg',
        alt: "badge'",
        title: '',
      },
      {
        certificateLink: '',
        class: 'psm cert-badge',
        badgeLink: 'assets/imgs/scurm.png',
        alt: "badge'",
        title: '',
      },
      {
        certificateLink: '',
        class: 'psm cert-badge',
        badgeLink: 'assets/imgs/cornell.png',
        alt: "badge'",
        title: '',
      },
      {
        certificateLink: '',
        class: 'psm cert-badge',
        badgeLink: 'assets/imgs/e.jpg',
        alt: "badge'",
        title: '',
      },
      {
        certificateLink: '',
        class: 'psm cert-badge',
        badgeLink: 'assets/imgs/ameriacn.jpg',
        alt: "badge'",
        title: '',
      },
      {
        certificateLink: '',
        class: 'psm cert-badge',
        badgeLink: 'assets/imgs/ieee.png',
        alt: "badge'",
        title: '',
      },
    ],
  },

  nav: {
    linkText: {
      title1: 'Home',
      linkHome: '#home',
      title2: 'About',
      linkAbout: '#about',
      title3: 'Resume',
      linkResume: '#resume',
      title4: 'Portfolio',
      linkPortfolio: '#portfolio',
      title5: 'Blog',
      linkBlog: '#blog',
      title6: 'Contact',
      linkContact: '#contact',
    },
    avatarLink: 'assets/imgs/asif.jpg',
    avatarTitle: 'Program Manager',
  },

  about: {
    summary: {
      title: 'Who am I ?',
      button: 'Download My CV',
      aboutTitle:
        'A Program Manager, Project Manager and an expert for Agile Transformation',
      short:
        '<div _ngcontent-pof-c118="" id="myElm" class="mt-20"> A lifelong learner, with <b _ngcontent-pof-c118="">14+ years</b> of program, product & project management experience in mission critical environments leading multi-year initiatives to implement modern CRM platforms. Collaborative, approachable & a persuasive communicator for project delivery and conflict resolution.<span _ngcontent-pof-c118="" id="dots0" style="display: none;">..</span>',
      detail:
        '<div _ngcontent-pof-c118="" id="more0" class=""><span _ngcontent-eic-c112=""> Strong believer in the CAN-DO mindset, and an unshakably optimistic team player. Program & project management experience with scaled Agile, ITIL & Waterfall methodologies. Certified Project management Professional (PMP), expert in developing SOP for service operations. Mentor project teams as an Agile Coach for enterprise - wide Scaled Agile (SAFe) adaptability. Lead program effort by establishing product vision, strategic roadmap, prioritized MVP features Enjoy articulating business workflows, cases & cause-effect impact through comprehensive scope of work. Manage cross-functional teams between implementation partners promoting a “customer first" environment. Enjoy problem solving by active listening, root cause analysis and setting-up clear expectations. Recognized for creating a team culture of value driven requirement elicitation. Collaborate with executive committee to define program business case, resource & budget plans. Define & drive program success criteria to improve software-defined infrastructure (SDI). Coach the scrum teams (establish scrum-of-scrums) for becoming self-organized and collaborative to deliver quality product. Drive project & manage vendors for customer success through KPI, SLA’s and Operation level agreements. Expert in leading teams by analyzing release milestone between Dev, QA, UAT, CI/CD production ready pipelines. Take pride in transforming resources into high performing self-organizing teams and becoming champions at cross functional collaborations for project success. I trust in my resources and conduct work-life with empathy and ethical accountability - and off course "the office" is my all-time favorite tv shows :)</div><button _ngcontent-pof-c118="" href="" onclick="readMore({dts: \'dots0\', mt: \'more0\', btn: \'rm0\'})" id="rm0" class="no-btn text-danger"></button></div>',
    },
    personalInfo: {
      title: 'Personal Info',
      heading1: 'email :',
      email: ' iam@asifsheraz.com',
      heading2: 'Phone :',
      phone: '443-405-5688 ',
      heading3: '',
      skype: '',
    },
    expertiese: {
      title: 'My Expertise',
      description: [
        {
          class: 'ti-cloud icon-lg',
          title: 'Orchestrating Project Management Office',
          subtitle: 'Agile, ITIL & Waterfall methodologies etc.',
        },
        {
          class: 'ti-stats-up icon-lg',
          title: 'Scaled Agile Framework for Application development',
          subtitle: 'Web, Mobile, Desktop, IoT and more...',
        },
        {
          class: 'ti-paint-bucket icon-lg',
          title: 'Technology Evangelism',
          subtitle:
            'Servant leadership, knowledge sharing, writing and more...',
        },
      ],
    },
  },

  resume: {
    mainTitle: 'Resume',
    heading1: 'Work History',
    heading2: 'Education',
    heading3: 'Hours Spent',
    heading4: 'Distinguished Aptitude',

    workHistory: [
      {
        Link: '',
        imageLink: 'assets/imgs/pmo.jpg',
        alt: '',
        title: 'Senior Project Manager, PMO Agile Lead  ',
        subtitle: 'Maryland Department of Human Services · Contract',
        date: 'Jan 2020 - Present',
        status: 'Baltimore, Maryland ',
        short:
          '<div _ngcontent-eic-c112="" class="description"> As Agile delivery manager, I manage 11 projects, coach, and mentor 7 teams, a total of 84 people. Collaborate with stakeholders as PMO lead to define & drive program increment (PI) for Agile Release train and control 100% of scope creep via change request. Coach and mentor functional scrum teams by utilizing test driven development and establishing CI/CD deployment pipelines that improved defect reduction by 21%. Effectively segregated the program vision into features / epics & established clarity on product roadmap which improved 27% of sprint commitments (deliverables).<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""><li _ngcontent-eic-c112=""> Established Scrum of Scrums as a part of scaled Agile framework  </li><li _ngcontent-eic-c112=""> Improved 17% of the program increment planning by articulating clear Definition of Done (DoD)</li><li _ngcontent-eic-c112="">  Created a culture of test driven development by delivering customer focused MVP features</li><li _ngcontent-eic-c112=""> Focused on user stories that complete the product increment (PI) cycle</li><li _ngcontent-eic-c112=""> Resulted in successful on-time release & deployment for all the 24 jurisdictions of Maryland</li><li _ngcontent-eic-c112=""> Highlight the program risk areas Vs. product roadmap </li><li _ngcontent-eic-c112=""> Mentor to establish core scrum by essential Agile Scrum activities</li> <li _ngcontent-eic-c112=""> Coach for Backlog grooming / refinement, Sprint Planning, Daily Stand-up (daily scrum), </li> <li _ngcontent-eic-c112=""> Established a culture to engage stakeholders for Sprint Review, Retrospectives and demos.</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management · ',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/penlogo.jpg',
        alt: '',
        title: 'Project Manager, Agile Coach ',
        subtitle:
          'State Employees Retirement System, Commonwealth of Pennsylvania',
        date: 'Jan 2019 - Jan 2020 ',
        status: 'Harrisburg, Pennsylvania Area',
        short:
          '<div _ngcontent-eic-c112="" class="description"> Established overall program governance outlining, readiness, cadence, roadmaps, resources alignment and cross functional ownerships – resulted in improving project completion with 22%.Manage projects valued $3.5M & reduced resource hours by 20%, & saved $750K in OPEX. Managed 5 Scrum teams (47 resources) & instituted product increment (PI) cadence to 5 sprints / PI.Orchestrated Scrum (ceremonies), and cross-functional team collaboration to overcome impediments<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""> <li _ngcontent-eic-c112="">  Engineered performance transparency of feature readiness via backlog governance, product level retrospectives and risks / dependencies and improved PI deliverable to 33% </li> <li _ngcontent-eic-c112="">Coached agile scrums by enabling teams to self-organize, cross function, and better collaborate</li> <li _ngcontent-eic-c112="">  Collaborated with product owner to groom the product backlog and remove impediments</li> <li _ngcontent-eic-c112="">  Utilized velocity and burndown charts to evaluate performances across the enterprise for improvements</li></ul></div> <button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/seniorManager.jpg',
        alt: '',
        title: 'Senior Manager (Agile Transformation)',
        subtitle:
          'Blue Cross and Blue Shield of Illinois, Montana, New Mexico, Oklahoma & Texas · Full-time',
        date: 'Feb 2017 - Jan 2019',
        status: 'Princeton, New Jersey, United States',
        short:
          '<div _ngcontent-eic-c112="" class="description"> Managed 3 agile teams, (19 resources) executed parallel sprints with quick sprint reviews. Planned sprints with user story points as unit to estimate the complexity of work over available capacity. Ensured bug tracking and issue tracking are aligned with user stories and JIRA dashboard is up to date. Utilized the requirement traceability matrix as a scale for scope creep and bugs that initiate triage <span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112="">  <li _ngcontent-eic-c112="">   Conducted daily standup, removed impediments, facilitated decision making for conflict resolution </li><li _ngcontent-eic-c112="">Engineered triage decision mechanism around BA, Test & Devs reducing unattended items to 15%</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/unilogo.png',
        alt: '',
        title: 'Senior Project Consultant',
        subtitle: 'Harrisburg, Pennsylvania',
        date: 'Oct 2016 - Feb 2017 ',
        status: 'Pennsylvania, United States',
        short:
          '<div _ngcontent-eic-c112="" class="description"> Established a POC for State-College Budget Office, and the Office of Student Financial Aid of $1.5M. Revalidated requirements & re-engineered business processes with wireframes. Annual delivered value Savings of $500K and 2K hours of resource time. Provided continuous functional support for tuition rate processes (SIS) for Jenzabar integration.Established assessment (SLA & KPI) by collaborating with technical and functional stakeholders. Helped brainstormed mapping logics to implement workflows using PeopleSoft and Tableau.<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""> <li _ngcontent-eic-c112=""> Documented system configurations for SIS/Supporting Ancillary Systems</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/seniorPM.jpg',
        alt: '',
        title: 'Project Manager',
        subtitle: 'Lysys. SA ',
        date: 'Mar 2015 – Sep 2016',
        status: 'Athens, Greece',
        short:
          '<div _ngcontent-eic-c112="" class="description">	A <b _ngcontent-pof-c118="">$2.3M </b>project, with annual delivered savings of <b _ngcontent-pof-c118="">$400K  </b>and <b _ngcontent-pof-c118="">15K</b> hours of resource time. Managed this enterprise level program with parallel phases of complex cross-functional collaboration. Lead the client facing team to elicit, validate, analyze the change of scope and document workflows. Established, guided, and scaled a matrixed project team comprised of multiple areas of business integration. Empowered developers for design discussions, requirements analysis and R&D to problem solving.<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""><li _ngcontent-eic-c112=""> Drove alignment, accountability, and collaboration to ensure delivery of commitments </li><li _ngcontent-eic-c112=""> Mentored business analysts to produced BRD, user stories, FRD, user guides & SOP unified artifacts</li><li _ngcontent-eic-c112=""> Documented detailed design of system changes along with application design and functional artifacts</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/ITsManager.jpg',
        alt: '',
        title: 'Operations Manager (ITS)',
        subtitle: 'Cornell University (WCM) ',
        date: 'Feb 2012 – Feb 2015 ',
        status: 'Ithaca, New York',
        short:
          '<div _ngcontent-eic-c112="" class="description">Lead a trusted-advisor approach to build relationships at all university levels to collaborate with diverse groups. Collaborated with C-Level executive committee and university partners to build and define program goals. Managed a project budget of <b _ngcontent-pof-c118=""> $3M</b>. Saved a yearly expense of <b _ngcontent-pof-c118="">$650K </b>over discontinued support. Lead a team of <b _ngcontent-pof-c118=""> 8</b> resident staff engineers and <b _ngcontent-pof-c118="">15 </b> sub-contractors to maintain instructional technologies. Served as a core member of PMO team for projects between Cornell, Sloan Kettering & QF – MEEZA. Created initial project estimates, managing scope and expectation (KPI/SLA) throughout the lifecycle.<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""><li _ngcontent-eic-c112="">Estimated impact of change on the project’s schedule and cost, by using parameters for risk analysis </li><li _ngcontent-eic-c112="">Developed SOP for support operations and driving project success through KPI, SLAs and OLA	</li><li _ngcontent-eic-c112="">Collaborate with steering committee to recommend policy, business process, & software requirements</li><li _ngcontent-eic-c112="">Developed Business Use Cases, Business Rules, logical analysis and change variation requirements </li> <li _ngcontent-eic-c112="">Facilitated as Agile Coach to develop operational KPI’s to adapt Scaled Agile Framework (SAFe)</li><li _ngcontent-eic-c112="">Led alignment, accountability, and collaborations to ensure delivery of project milestone & commitments</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/prodManger.jpg',
        alt: '',
        title: 'Product Manager ',
        subtitle: 'ABB Asea Brown Boveri Ltd (Zurich) ',
        date: 'Jul 2009 – Jan 2012',
        status: 'Frankfurt, Germany',
        short:
          '<div _ngcontent-eic-c112="" class="description">Served as a Product Manager for 4 MENA counties for managing project teams, budget, and schedule. Increased annual system upgrade revenue with 14% by developing after sales support wing. Managed a team of 8 resident ELV engineers and a team of 25 upstream engineers. Created requirement traceability matrix (RTM) to manage scope creep and project scope variation.<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""><li _ngcontent-eic-c112="">Maintained single-line communication with stakeholders to prioritize and schedule projects</li><li _ngcontent-eic-c112="">Used sales force for change management, problem management and incident management</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/systemEng.jpg',
        alt: '',
        title: 'System Engineer ',
        subtitle: 'Cyber-Consult FZN 	 ',
        date: 'Feb 2007 – July 2009',
        status: 'Dubai, UAE',
        short:
          '<div _ngcontent-eic-c112="" class="description">Defined project scope, goals, detailed requirements in collaboration with senior leadership and stakeholders. Utilized UML for visual modeling and a detailed use-case specification to describe the design, architecture, and dynamic behavior of the heat mapping & thermal imagining. Designed, installed & maintained incident and parameter detection system. Developed DDD (detail design document) including requirements for installation & commissioning. Established an effective single line of communication with production & shutdown processors.<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> <ul _ngcontent-eic-c112=""><li _ngcontent-eic-c112="">Lead project sites for implementing IDF-MDF points, long-range radio links & Low Voltage systems</li><li _ngcontent-eic-c112="">Designed detailed system schematics for switching & control using UML, AutoCAD and SIT</li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          ' Management Systems ·',
          'Defect Tracking ·',
          'IT Project & Program Management ·',
          'Agile Project Management ·',
          ' Business Process Improvement · ',
          ' Project Planning',
        ],
      },
    ],
    education: [
      {
        qualification: 'MS. Information Systems, Pennsylvania',
        university: 'Harrisburg University ',
      },

      {
        qualification: 'BS. Computer Science, ISB ',
        university: 'Preston University',
      },

      {
        qualification:
          'Entrepreneurship/Entrepreneurial Studies, Massachusetts',
        university: 'Massachusetts Institute of Technology',
      },
      {
        qualification: 'Project Management, New York ',
        university: ' American Management Association',
      },
      {
        qualification: 'Higher-Ed Program Management, Florida ',
        university: ' Educause',
      },
      {
        qualification: 'Research BI over Business Processes, New York ',
        university: ' IEEE ',
      },
    ],
    skills: [
      {
        name: 'Kubernetes',
        score: '97%',
      },
      {
        name: 'JavaScript',
        score: ' 95%',
      },
      {
        name: 'Nodejs',
        score: '95%',
      },
      {
        name: 'MongoDB',
        score: '90%',
      },
      {
        name: 'Angular',
        score: '97%',
      },
      {
        name: 'Python',
        score: '80%',
      },
      {
        name: 'Flutter',
        score: '85%',
      },
      {
        name: 'React',
        score: '85%',
      },
      {
        name: 'Dot Net',
        score: '80%',
      },
      {
        name: 'SQL',
        score: '80%',
      },
      {
        name: 'DevOps',
        score: '95%',
      },
      {
        name: 'DevSecOps',
        score: '85%',
      },
    ],
    aptitude: [
      {
        title: 'Team Building',
        score: '95%',
      },
      {
        title: 'Product Management',
        score: '95%',
      },
      {
        title: 'Servant Leadership',
        score: '100%',
      },
      {
        title: 'System Design',
        score: '93%',
      },
      {
        title: 'Presentation',
        score: '93%',
      },
    ],
  },

  banValues: [
    {
      class: 'ti-alarm-clock icon-xl',
      score: '50%',
      name: 'Servant Leadership',
    },
    {
      class: 'ti-layers-alt icon-xl',
      score: '10%',
      name: 'Confliction resolution',
    },
    { class: 'ti-face-smile icon-xl', score: '30%', name: 'Mentorship' },
    {
      class: 'ti-heart-broken icon-xl',
      score: '10%',
      name: 'Knowledge Sharing',
    },
  ],

  projects: {
    highlightedTitle: 'Freelance ',
    Title: ' Projects',

    myWork: [
      {
        icon: 'ti-vector text-danger',
        name: 'KAIOPS.io ',
        spot: 'UI/UX consultant for the Kubernetes cluster orchestration dashboard',
        details: '<p _ngcontent-gtt-c110="" class="mh-18x"> </p>',
      },
      {
        icon: 'ti-bar-chart text-danger',
        name: 'University System of Maryland ',
        spot: ' Enterprise-wide Agile transformation for UMBC, Salisbury university',
        details: '<p _ngcontent-aty-c110="" class="mh-18x"> </p>',
      },
      {
        icon: 'ti-bar-chart text-danger',
        name: 'Techila.io ',
        spot: 'Product consultant for articulating pitch-decks for the seed-level investments  ',
        details: '<p _ngcontent-jkv-c110="" class="mh-18x"> </p>',
      },
      {
        icon: 'ti-support text-danger',
        name: 'PMP Agile training (Baltimore Chapter) ',
        spot: 'Trainer and mentor for Project management and Agile facilitation ',
        details: '<p _ngcontent-srx-c110="" class="mh-18x">  </p>',
      },
    ],
  },

  availabilityBanner: {
    position: 'I Am Available as a day one contribution',
    buttonText: 'Get in touch',
  },

  articles: {
    highlightedTitle: 'Latest',
    title: ' Articles',
    blogLink: '',
    buttonText: 'View All',

    blog: [
      {
        blogLink: '',
        name: 'Arslan Ali',
        imageLink: 'assets/imgs/blog3.png',
        alt: 'blog',
        title:
          'Application Deployment using Gitlab CI/CD on Managed Kubernetes Cluster at GCP',
        Link2:
          'https://faun.pub/application-deployment-using-gitlab-ci-cd-on-managed-kubernetes-cluster-at-gcp-72b59496979c',
        paragraph2:
          "In continuation of my previous article where I've deployed the same mono-repo of microservices application on Docker Swarm instead of Kubernetes, we will repeat most of the steps and I will try to be brief about all the steps except the deployment configuration. So it is highly recommended that you read this article first.",
        likes: '54',
        comments: '2',
        paragraph: {
          firstlines: 'Step by Step guide to deploy a',
          secondlines: ' on GCP using Gitlab CI/CD Pipelines.',
          link: 'https://github.com/dockersamples/example-voting-app',
          bold: true,
          linktext: 'mono-repo of microservices',
        },
      },
    ],
  },
  portfolio: {
    highlightedTitle: 'Opensource',
    title: 'Portfolio (Work in progress 😊 )',
    links: [
      { title: 'New', link: '', data: '.new', class: 'active' },
      { title: 'Security', link: '', data: '.advertising', class: '' },
      { title: 'DevOps', link: '', data: '.branding', class: '' },
      { title: 'Others', link: '', data: '.web', class: '' },
    ],
    itemCard: [
      {
        class: 'item long card',
        imageLink: 'assets/imgs/blog1.jpg',
        Link: 'https://gitlab.com/',
        header: 'Services Mono Repo Starter',
        body: 'A mono-repo for Multi Language microservices based application skeleton,built with Angular and React Frontends, nginx for serving static assets and proxing apirequests, NestJs for backend Gateway and DB Workers.',
      },
      {
        class: 'item card',
        imageLink: 'assets/imgs/web-1.jpg',
        Link: 'https://gitlab.com',
        header: 'Services Mono Repo Starter',
        body: 'A mono-repo for Multi Language microservices based application skeleton,built with Angular and React Frontends, nginx for serving static assets and proxing apirequests, NestJs for backend Gateway and DB Workers.',
      },
    ],
    portfolioItem: [
      {
        class: 'col-md-6 col-lg-4 web new',
        imageLink: 'assets/imgs/web-2.jpg',
        title: ' WEB',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising new',
        imageLink: 'assets/imgs/advertising-2.jpg',
        title: ' ADVERSTISING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 web',
        imageLink: 'assets/imgs/web-4.jpg',
        title: ' WEB',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising',
        imageLink: 'assets/imgs/advertising-1.jpg',
        title: ' advertising',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 web new',
        imageLink: 'assets/imgs/web-3.jpg',
        title: ' WEB',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising new',
        imageLink: 'assets/imgs/advertising-3.jpg',
        title: ' ADVERSITING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising new',
        imageLink: 'assets/imgs/advertising-4.jpg',
        title: ' ADVERSITING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-2.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-3.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-4.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-5.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
    ],
  },

  review: {
    highlightedTitle: 'Client',
    Title: 'Reviews',
    values: [
      {
        class: 'carousel-item active',
        paragragh:
          'sif was a joy to work with at WCM-Q in Qatar. As a colleague, he was supportive and always willing to provide ideas for our challenging problems. He managed his team with a collaborative leadership style and was able to get the best our of his reports. Since his team interfaced directly with faculty, in Mission critical work, he ensured that the customer experience was central to the vision of his team. He is a person who loved to experiment and he always faced challenges head on. I look forward to the opportunity to work with him again. ',
        imageLink: 'assets/imgs/yusaf.jpg',
        name: 'Yusuf S.',
        detail: 'Director, Information Systems',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I had the privilege of working with Asif on a project at SERS where I was one of the Senior developers. Asif’s positivity is truly commendable. He motivates and mentors his team to continuously enhance their productivity. His ability to understand the needs of the customer, communication skills both internal and external, strong backlog prioritization and Scrum Master skills have helped in building teams and in improving efficiency of teams. Asif is a pleasure to work with on any project. His outstanding knowledge and unique skills are valuable to any business looking to scale their products using Agile and its frameworks.I had the privilege of working with Asif on a project at SERS where I was one of the Senior developers. Asif’s positivity is truly commendable. He motivates and mentors his team to continuously enhance their productivity. His ability to understand the needs of the customer, communication skills both internal and external, strong backlog prioritization and Scrum Master skills have helped in building teams and in improving efficiency of teams. Asif is a pleasure to work with on any project. His outstanding knowledge and unique skills are valuable to any business looking to scale their products using Agile and its frameworks.',
        imageLink: 'assets/imgs/raja.jpg',
        name: 'Raja Chamarthi ',
        detail: 'Java Full Stack | AWS | Microservices | Springboot | Angular',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I work with Mr. Asif Sheraz on a number of projects. He is a soft spoken, polite and cooperative individual. He was very cooperative each time we worked on something and has good customer skills. He is punctual and a professional. He will be a good candidate to work in any organization. ',
        imageLink: 'assets/imgs/ahmed.jpg',
        name: 'Ahmed Hasnain   ',
        detail:
          'Student Outreach and Educational Development Manager at Cornell University',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I worked under Asif management and I found him a thorough professional who knows how to come out of the pressure situations and get the work done. I believe a real manager must interact with his team to get the confidence and trust of the team players and this is one of the key characteristics Asif possesses. Asif is a real team leader who love to communicate with the team and knows the strengths and weaknesses of his team players, and that is the key for any successful leader. I wish him all the best for his future roles.',
        imageLink: 'assets/imgs/muhamad.jpg',
        name: 'Muhammad Waqas',
        detail: 'Business Intelligence Team Lead at Teradata',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I have had the pleasure of working with Asif as a Manager (SME relationship in both Waterfall or AGILE project environments). His knowledge of outreach and ability to connect with stakeholders, project key players for information gathering, elicitation and escalation handling is unmatched. He is very committed to his work, extremely resourceful and competent for AGILE, Sprint planning & Backlog management. ',
        imageLink: 'assets/imgs/saim.jpg',
        name: 'Saaim Aslam',
        detail:
          'Keynote Speaker | Digital Transformation | Innovation | Emerging Technology | CIO/CTO/CEO advisory',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'Asif is a very smart and energetic person to work with. He has a great zeal and dedication towards his work. Always passionate on the latest technologies and willing to learn new things and take new challenges. He is a good team player who live by the idea of knowledge transfer and help assist other operational team members. I wish him all the best in his career.',
        imageLink: 'assets/imgs/jeffry.jpg',
        name: 'Jeffrey Garrovillas',
        detail: 'Technical Lead at Informatica Qatar',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I have worked with Asif for more than 3 years, and my consistence experience with him has been one of respect, trust, stability and dedication. His excellent technical background and fine sense for resource management is a great combination for an IT architect & consultant for any technology driven company. His has the ability to work as a key technical account manager to manage complex solutions for major operational clients. Asif has excellent awareness of different cultures, work ethics and has the willingness the go the extra mile, not just within his own area of responsibility but with any action that helps the company ahead – I wish Asif all the best of luck.',
        imageLink: 'assets/imgs/waqas.jpg',
        name: 'Waqas ali  ',
        detail: 'Principal Consultant ICT - BEng, RCDD, CCNA',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I worked with Asif as a EPC vendor for ABB automation system upgrade project for Qatar Petroleum Refinery- NGL3.He is extremely focused and hardworking person and possesses excellent technical communication skills. He has phenomenal work ethics and commitment to perform better even under great pressure and difficult situations. As a person he is a great colleague, an honest friend, an awesome confidant, responsible and a reliable individual.Professionally, he has direct knowledge of the mission critical functions & he is very successful in establishing an escalation point to resolve the issues and prioritize requests to meet desired daily operations, project deliverables & most importantly end- user expectations. I wish him best of luck in his current and future endeavors.',
        imageLink: 'assets/imgs/majid.jpg',
        name: 'Majid Alwaince   ',
        detail: 'Support & Services Manager',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'Asif sage wisdom has proved invaluable to a large scale project in which I was recently involved. I had the opportunity to work closely with Asif for close to a year in his role as a Project Coordinator and SCRUM Master and have witnessed his ability to view not only the larger image but to also quickly comprehend the many minuscule parts comprising the larger picture. It is because of this ability that he is also very astute at foreseeing potential obstacles and challenges well ahead of most others. Asif is undoubtedly skilled at not only seeing the big design and the moving parts, but also at connecting with his staff at a very real and honest level.Even though Asif role of managing entirely different teams with different needs and goals is obviously challenging, he performs his role almost effortlessly, all the while remaining optimistic and humble. It is Asifs humanness that I have come to immensely appreciate. He greets folks with a genuine smile, even during more difficult and stressful times of the project. Asif is a very down-to-earth individual while maintaining the utmost diplomacy and professionalism. I especially cherish the talks that we have had during some of my own trying times with the project and admire his honest and straightforward approach. I applaud his clear vision of knowing what needs to be done and how best to find solutions to accomplish a task or a goal. Asif possesses a clear vision of what it means to lead. I highly recommend Asif and truly believe that any employer would be lucky to have him calmly guide their ship through even the most tumultuous waters! ',
        imageLink: 'assets/imgs/rahn g.jpg',
        name: 'Rahn G.  ',
        detail: 'Training Development Consultant and Designer',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
      {
        class: 'carousel-item',
        paragragh:
          'I worked with Asif as a EPC vendor for ABB automation system upgrade project for Qatar Petroleum Refinery- NGL3.He is extremely focused and hardworking person and possesses excellent technical communication skills. He has phenomenal work ethics and commitment to perform better even under great pressure and difficult situations. As a person he is a great colleague, an honest friend, an awesome confidant, responsible and a reliable individual.Professionally, he has direct knowledge of the mission critical functions & he is very successful in establishing an escalation point to resolve the issues and prioritize requests to meet desired daily operations, project deliverables & most importantly end- user expectations. I wish him best of luck in his current and future endeavors.',
        imageLink: 'assets/imgs/majid.jpg',
        name: 'Majid Alwaince   ',
        detail: 'Support & Services Manager',
        icon: 'ti-linkedin',
        myLink: 'https://linkedin.com/in/asifsheraz',
      },
    ],
  },

  contacts: [
    { title: 'Phone', class: 'ti-mobile icon-md', value: ' 443-405-5688 ' },
    {
      title: 'Address',
      class: 'ti-map-alt icon-md',
      value:
        'Ellicott City, Maryland, United States of America, 21042, Planet Earth, The Solar System, Oort Cloud, Local Fluff, Local Bubble, Orion Arm, Milky Way Galaxy, Local Group, Virgo Supercluster, Laniakea Supercluster, Universe ',
    },

    {
      title: 'Email',
      class: 'ti-envelope icon-md',
      value: ' iam@asifsheraz.com',
    },
  ],
  footervalue: [
    {
      portfolioLink: 'https://arslanali.io',
      name: 'Arslan Ali',
    },
  ],

  socials: [
    {
      name: 'facebook',
      icon: 'ti-facebook',
      link: 'https://www.facebook.com',
    },
    {
      name: 'twitter',
      icon: 'ti-twitter',
      link: 'https://twitter.com',
    },
    {
      name: 'youtube',
      icon: 'ti-youtube',
      link: 'https://www.youtube.com',
    },
    {
      name: 'linkedin',
      icon: 'ti-linkedin',
      link: 'https://linkedin.com/in/asifsheraz',
    },
    { name: 'gitlab', icon: 'ti-github', link: 'https://gitlab.com' },
  ],
};
