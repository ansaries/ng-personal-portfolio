export const sumanData: ProfileNamespace.Persona = {
  mainBanner: {
    title: 'Hello, I am',
    yourName: 'Suman Gill',
    jobPositions: 'Software Architect | Developer',
    resumeLink: 'assets/cv/suman-resume.pdf',
    buttonText: 'Print Resume',
    portfolioLink: 'https://github.com/samanat26/',
    certification: [],
  },

  nav: {
    linkText: {
      title1: 'Home',
      linkHome: '#home',
      title2: 'About',
      linkAbout: '#about',
      title3: 'Resume',
      linkResume: '#resume',
      title4: 'Portfolio',
      linkPortfolio: '#portfolio',
      title5: 'Blog',
      linkBlog: '#blog',
      title6: 'Contact',
      linkContact: '#contact',
    },
    avatarLink: 'assets/imgs/suman.jpeg',
    avatarTitle: 'Software Architect | Developer',
  },

  about: {
    summary: {
      title: 'Who am I ?',
      button: 'Download My CV',
      aboutTitle:
        'An Engineering Manager, Software Architect, and a Cloud Native Development Expert',
      short:
        '<div _ngcontent-pof-c118="" id="myElm" class="mt-20"><b _ngcontent-pof-c118="">18+ years</b> of technology leadership experience, with more than 10 years in Core Software Development helping fast paced, high growth, and high tech startups developing <b _ngcontent-pof-c118="">B2B, B2C, AIOps and DevSecOps Products</b>.<span _ngcontent-pof-c118="" id="dots0" style="display: none;">..</span>',
      detail:
        '<div _ngcontent-pof-c118="" id="more0" class=""><ul _ngcontent-pof-c118=""><li _ngcontent-pof-c118=""> Worn different hats to perform <b _ngcontent-pof-c118="">Product Development, Budget Financials, SDLC, KPI, BI, Launch, Marketing, Customer Acquisition, Retention Policies, UI/UX, NFX</b> etc. </li><li _ngcontent-pof-c118=""> Demonstrated professional skills in planning, leading, aligning, and creating new <b _ngcontent-pof-c118="">monetizations</b> and <b _ngcontent-pof-c118="">revenue streams</b>. </li><li _ngcontent-pof-c118=""> Expert in creating, presenting and pitching the <b _ngcontent-pof-c118="">inclusion of cutting edge technologies, business plans, MVPs,</b> and different <b _ngcontent-pof-c118="">BI reports</b> to customer, partners, potential investors, other stackholders and even to general public in conferences. </li><li _ngcontent-pof-c118=""> Accomplished <b _ngcontent-pof-c118="">CKAD</b> certification, which helped leverage the containerization and orchestration technologies to build and deploy cloud native applications by utilizing <b _ngcontent-pof-c118="">kubernetes, Openshift, Dockers and Helm</b> etc. (Currently pursuing <b _ngcontent-pof-c118="">CKS</b> and <b _ngcontent-pof-c118="">CKA</b> certifications) </li><li _ngcontent-pof-c118=""> Experienced in building and deploying cloud native applications on <b _ngcontent-pof-c118="">AWS, Azure, GCP, IBM Cloud, Oracle Cloud, Alibaba Cloud, Digital Ocean, Heruku </b> etc. </li><li _ngcontent-pof-c118=""> Scored <b _ngcontent-pof-c118="">PSM (Professional Scrum Master)</b> certification to implement effective change management and agile methodologies to deliver high quality <b _ngcontent-pof-c118="">Adaptive and Resilient products</b> leading and working with global cross functional remote teams. </li></ul></div><button _ngcontent-pof-c118="" href="" onclick="readMore({dts: \'dots0\', mt: \'more0\', btn: \'rm0\'})" id="rm0" class="no-btn text-danger"></button></div>',
    },
    personalInfo: {
      title: 'Personal Info',
      heading1: 'email',
      email: 'iam@arslanali.io',
      heading2: 'phone',
      phone: '+1 301 7414 229',
      heading3: 'skype',
      skype: 'ansaries',
    },
    expertiese: {
      title: 'My Expertise',
      description: [
        {
          class: 'ti-cloud icon-lg',
          title: 'Cloud Native Designs',
          subtitle: 'Microservices, serverless, CICD, TDD, DDD etc.',
        },
        {
          class: 'ti-stats-up icon-lg',
          title: 'Application Development',
          subtitle: 'Web, Mobile, Desktop, IoT and more...',
        },
        {
          class: 'ti-paint-bucket icon-lg',
          title: 'Technology Evangelism',
          subtitle:
            'Servant leadership, knowledge sharing, writing and more...',
        },
      ],
    },
  },

  resume: {
    mainTitle: 'Resume',
    heading1: 'Work History',
    heading2: 'Education',
    heading3: 'Hours Spent',
    heading4: 'Distinguished Aptitude',

    workHistory: [
      {
        Link: 'https://kaiops.io',
        imageLink: 'assets/imgs/kaiops.jpeg',
        alt: 'kaiops_logo',
        title: 'Engineering Manager',
        subtitle: 'Kaiops.io',
        date: '2021 - Present',
        status: 'Virginia, USA (Full Time Remote)',
        short:
          '<div _ngcontent-eic-c112="" class="description"> Leading AIOPs development studio, extending the functionality of Kubernetes and bringing AI/ML to the core of its clusters. Kaiops is a SaaS with minimal deployment footprint in your cluster and provides Efficient Capacity Management, Security and Compliance, Realtime Cluster Events Monitoring, and runs AI models to optimize and automate the cluster resource utilization.<span _ngcontent-eic-c112="" id="dots1">...</span>',
        more: '<div _ngcontent-eic-c112="" id="more1" class=""><span _ngcontent-eic-c112=""> Wearing different hats and having a go-go-go attitude towards solving challenges was the most amazing part of my time at Kaiops. Following are some of the most important tasks where I enjoyed challenging myself to go over and beyond my limits: </span><ul _ngcontent-eic-c112=""><li _ngcontent-eic-c112=""> I started off with designing and architecting the SaaS infrastructure. </li><li _ngcontent-eic-c112=""> Rolled-out an <strong _ngcontent-eic-c112="">MVP</strong> in less than <strong _ngcontent-eic-c112="">2 months</strong> with the help of an enthusiastic team and also by utilizing the enterprise SaaS modules and boilerplates I developed over the years. </li><li _ngcontent-eic-c112=""> Implemented the overall hiring and onboarding process for the engineering team members which include; the creation of JDs, <strong _ngcontent-eic-c112="">Posting Ads</strong> on Portals, <strong _ngcontent-eic-c112="">initial screening</strong> through automated test platforms, <strong _ngcontent-eic-c112="">Interviewing</strong>, <strong _ngcontent-eic-c112="">onboarding</strong> and <strong _ngcontent-eic-c112="">training</strong>, etc. </li><li _ngcontent-eic-c112=""> Implemented formal <strong _ngcontent-eic-c112="">Agile Practices</strong> through <strong _ngcontent-eic-c112="">sprint planning</strong>, <strong _ngcontent-eic-c112="">daily scrums</strong>, and <strong _ngcontent-eic-c112="">sprint review</strong> meetings </li><li _ngcontent-eic-c112=""> Facilitated the founders in Market Analysis, Forecasted Financials, competitors metrics, and even monetization plan. </li><li _ngcontent-eic-c112=""> Conducted <strong _ngcontent-eic-c112="">Live Demos</strong> to customers/potential partners and was involved in most of the Investor Pitches. </li><li _ngcontent-eic-c112=""> Created a mono-repo using NRWL for hosting multiple microservices, different reusable libraries, and other third-party modules. </li><li _ngcontent-eic-c112=""> Developed <strong _ngcontent-eic-c112="">CICD pipelines</strong> for multiple environments, implemented caching, and automation, and managed application configs/secrets on Gitlab while integrating GKE and Amazon EKS. This brings us to less than <strong _ngcontent-eic-c112="">15min of release cycle time</strong>. </li><li _ngcontent-eic-c112=""> Created many UI Features, and Dashboard widgets using <strong _ngcontent-eic-c112="">AngularV13</strong> and <strong _ngcontent-eic-c112="">D3</strong> Engines. </li><li _ngcontent-eic-c112=""> Improved frequent DB and interservice grpc calls response by <strong _ngcontent-eic-c112="">660%</strong> by developing a custom multi- layer caching engine. </li><li _ngcontent-eic-c112=""> Designed and Developed <strong _ngcontent-eic-c112="">Optimized Storage Engine</strong> which helped save only the essential time- series telemetry data and even optimize and aggregate the data over time resulting in improving <strong _ngcontent-eic-c112="">10x Storage</strong> space and <strong _ngcontent-eic-c112="">3x IOPs</strong> improvement. </li><li _ngcontent-eic-c112=""> Written multiple <strong _ngcontent-eic-c112="">Code Generators</strong>, that helped teams generate multi-language standardized code in minutes which otherwise could have taken days. </li><li _ngcontent-eic-c112=""> Created 100s of <strong _ngcontent-eic-c112="">Docker, Kubernetes,</strong> and <strong _ngcontent-eic-c112="">Helm manifests</strong> to help deploy Kaiops <strong _ngcontent-eic-c112="">Microservices</strong> and also to deploy agent services on customer clusters. </li><li _ngcontent-eic-c112=""> Developed Application wide Dynamic Audit Log by creating custom <strong _ngcontent-eic-c112="">Typescript Decorators</strong> in Nestjs. </li></ul></div><button _ngcontent-eic-c112="" href="" onclick="readMore({dts: \'dots1\', mt: \'more1\', btn: \'rm1\'})" id="rm1" class="no-btn text-danger"> </button></div>',

        tags: [
          'Team Leadership',
          'Team Building',
          'Project Management',
          'Servant Leadership',
          'Product Management',
          'System Design',
          'Cloud Security',
          'Typescript',
          'Angular',
          'React',
          'Python',
          'Nodejs',
          'Kubernetes',
          'Helm',
          'Bash',
          'DevOps',
          'DevSecOps',
          'AI/ML',
          'Redis',
          'Kafka',
          'MongoDb',
          'TimescaleDB',
          'PostgresSQL',
          'GKE',
          'EKS',
          'Gitlab Gitops',
          'Kubeclarity',
          ' Prometheus',
        ],
      },
      {
        Link: 'https://goboilerquotes.com',
        imageLink: 'assets/imgs/boiler.jpeg',
        alt: 'boiler_logo',
        title: 'Engineering Manager',
        subtitle: 'GoBoilerQuotes.com',
        date: '2019 - 2021',
        status: 'London, UK (Full Time Remote)',
        short:
          '<div _ngcontent-gdx-c112="" class="des"> Leading and Managing a Team of cross-border Engineers for the Development of this SaaS product for the European Markets. Incorporated my passion for Cloud-Native Application Development: designed and currently developing along with my team this amazing product using the event-driven microservices architecture. <span _ngcontent-gdx-c112="" id="dots2">...</span>',
        more: '<div _ngcontent-gdx-c112="" id="more2" class=""> Boiler Quotes is an online platform developed for house owners in UK to service, repair or install new boilers in their homes by selecting Gas Safe® Certified Engineers. In creation of this outstanding product, I am leading and have completed following tasks: <ul _ngcontent-gdx-c112=""><li _ngcontent-gdx-c112=""> Technical review of Corporate plan and help create effective and efficient <strong _ngcontent-gdx-c112="">Go To Market</strong> strategy. </li><li _ngcontent-gdx-c112=""> Planning, designing &amp; deploying Engineering policies and standards. Implementing code &amp; development, <strong _ngcontent-gdx-c112="">CICD</strong>, <strong _ngcontent-gdx-c112="">testing</strong>, <strong _ngcontent-gdx-c112="">quality assurance</strong> and <strong _ngcontent-gdx-c112="">security standards</strong> ensuring effective delivery procedures </li><li _ngcontent-gdx-c112=""> Leading <strong _ngcontent-gdx-c112="">Cloud Native</strong> Microservices Architecture, leveraging the AWS AKS/GKE infrastructure to host and orchestrate distributed microservices on hosted Kubernetes Engine. </li><li _ngcontent-gdx-c112=""> Creating Microservices Development Architecture based on <strong _ngcontent-gdx-c112="">Nodejs</strong>, <strong _ngcontent-gdx-c112="">Dot Net</strong> and <strong _ngcontent-gdx-c112="">Spring Frameworks.</strong></li><li _ngcontent-gdx-c112=""> Developing Boilerplate Microservices Projects to help take the development process off the ground for the Development teams using following Technology Stack: <ol _ngcontent-gdx-c112=""><li _ngcontent-gdx-c112=""><strong _ngcontent-gdx-c112="">Event Sourcing</strong>, <strong _ngcontent-gdx-c112="">Caching</strong>, <strong _ngcontent-gdx-c112="">Message Queue</strong> using <strong _ngcontent-gdx-c112="">Redis</strong></li><li _ngcontent-gdx-c112=""> Image and File Storage at <strong _ngcontent-gdx-c112="">AWS S3</strong>, <strong _ngcontent-gdx-c112="">MongoDB</strong> and <strong _ngcontent-gdx-c112="">Postgres SQL</strong></li><li _ngcontent-gdx-c112=""> Frameworks stack contain and not limited to <strong _ngcontent-gdx-c112="">Nestjs</strong>, <strong _ngcontent-gdx-c112="">Dot Net</strong>, <strong _ngcontent-gdx-c112="">Spring</strong>, <strong _ngcontent-gdx-c112="">Angular</strong>, <strong _ngcontent-gdx-c112="">Nrwl</strong>, <strong _ngcontent-gdx-c112="">Typescript</strong>, <strong _ngcontent-gdx-c112="">Express</strong>, <strong _ngcontent-gdx-c112="">Fastify</strong>, <strong _ngcontent-gdx-c112="">GRPC</strong>, <strong _ngcontent-gdx-c112="">Nest Cloud</strong> etc. </li><li _ngcontent-gdx-c112=""> Deployment and Containerization is done using <strong _ngcontent-gdx-c112="">Docker</strong> and <strong _ngcontent-gdx-c112="">Helm</strong> for <strong _ngcontent-gdx-c112="">Kubernetes</strong></li><li _ngcontent-gdx-c112=""><strong _ngcontent-gdx-c112="">CICD</strong> using Gitlab Gitops</li><li _ngcontent-gdx-c112=""> Automation and Deployment Scripts/Manifests using <strong _ngcontent-gdx-c112="">Bash</strong> and <strong _ngcontent-gdx-c112="">Yaml</strong></li></ol></li><li _ngcontent-gdx-c112=""> Designing and developing 4 different levels of Caches using different caching algorithms to make the Application extremely fast in creating First Contentful Paint, First Full Load, Subsequent Loads, <strong _ngcontent-gdx-c112="">API Caching</strong> on both Server and Client Side. </li><li _ngcontent-gdx-c112=""> Projecting <strong _ngcontent-gdx-c112="">Lazy Loading</strong> at Component Level using Angular’s new <strong _ngcontent-gdx-c112="">Ivy Engine</strong> capabilities and designed a Fully Customizable Modular Widget/Component based Page loading algorithm. </li><li _ngcontent-gdx-c112=""> Leading and implementing <strong _ngcontent-gdx-c112="">RBAC</strong>, <strong _ngcontent-gdx-c112="">OAuth</strong> and <strong _ngcontent-gdx-c112="">JWT</strong> based Microservice for handling Authentication and Permissions. </li><li _ngcontent-gdx-c112=""> Creating Notification Microservice using Event Sourcing while integrating it with <strong _ngcontent-gdx-c112="">Amazon SES</strong>, <strong _ngcontent-gdx-c112="">SNS</strong>, <strong _ngcontent-gdx-c112="">Firebase Messaging</strong> and <strong _ngcontent-gdx-c112="">Message Bird</strong>. </li><li _ngcontent-gdx-c112=""> Building Worker Microservices for handling IO and other processing tasks while keeping the front-end Microservices loosely coupled using <strong _ngcontent-gdx-c112="">Redis MQ</strong></li><li _ngcontent-gdx-c112=""> Designing and deploying Payment, Accounting, Billing and other Microservices for keeping the separation of concern and modularity in place. </li></ul></div><button _ngcontent-gdx-c112="" onclick="readMore({dts: \'dots2\', mt: \'more2\', btn: \'rm2\'})" id="rm2" class="no-btn text-danger"></button></div>',
        tags: [
          'Team Leadership',
          'Team Building',
          'Project Management',
          'Servant Leadership',
          'Product Management',
          'System Design',
          'Typescript',
          'Angular',
          'React',
          'Python',
          'Nodejs',
          'Kubernetes',
          'Helm',
          'Bash',
          'DevOps',
          'DevSecOps',
          'AI/ML',
          'Redis',
          'MongoDb',
          'PostgresSQL',
          'GKE',
          'EKS',
          'Gitlab Gitops',
          'Kubeclarity',
          ' Prometheus',
        ],
      },
      {
        Link: 'https://fixonclick.com',
        imageLink: 'assets/imgs/fixonclick.jpeg',
        alt: 'kaiops_logo',
        title: 'CTO / Co-Founder',
        subtitle: 'Fixonclick.com',
        date: '2015 - 2019',
        status: 'Dubai, UAE (Full Time On-Site)',
        short:
          '<div _ngcontent-iix-c112="" class="de"> FixOnClick – was an online application developed under the hood of Dexterous Online Portals LLC in Dubai. FixOnClick App helped in connecting consumers to the required Professional Fixers and have them drop in right at their doorstep anywhere in UAE. Users simply sign-in and search for the services that they need. Fill out their requirements and watch a Professional Fixer assigned to them within minutes. Fixonclick was available on Google and App Store. <span _ngcontent-iix-c112="" id="dots3">...</span>',
        more: '<div _ngcontent-iix-c112="" id="more3" class=""><span _ngcontent-iix-c112=""> For highly dynamic architecture of the app, it was important to build it in-house that I orchestrated with my design, engineering, marketing and sales team. Following major roles with an enthusiastic team that built FixOnclick. </span><ul _ngcontent-iix-c112=""><li _ngcontent-iix-c112=""> Managed, hired, trained cross-functional local/remote engineering and customer support teams. </li><li _ngcontent-iix-c112=""> Designed the MVP to launch initially and later a modular state-of-the-art Software Architecture using Current Edge Technologies which helped updating features and shipping them in minimum possible time. </li><li _ngcontent-iix-c112=""> Designed Micro Services Architecture to fulfill a <strong _ngcontent-iix-c112="">12 Factor Application</strong> compliance, which helped developing the features in Sprints with iterations and keeping the separation of concerns. </li><li _ngcontent-iix-c112=""> Designing Core Services’ Architecture for Users, <strong _ngcontent-iix-c112="">Monetization</strong>, and a real-time Notification Engine. </li><li _ngcontent-iix-c112=""> Developed the Cloud Deployment Architectures to utilize the containerization and orchestration technologies like <strong _ngcontent-iix-c112="">Docker</strong>, <strong _ngcontent-iix-c112="">Docker Swarm</strong>, <strong _ngcontent-iix-c112="">Kubernetes</strong> and <strong _ngcontent-iix-c112="">Open Shift</strong> etc. to leverage the microservice’ deployments, scaling, monitoring, management and delivery. </li><li _ngcontent-iix-c112=""> Hands-on experience of developing backend micro-services using <strong _ngcontent-iix-c112="">Node</strong>, <strong _ngcontent-iix-c112="">Python</strong>, <strong _ngcontent-iix-c112="">Spring</strong> and <strong _ngcontent-iix-c112="">.Net</strong>. </li><li _ngcontent-iix-c112=""> Extensively developed cloud native boilerplates with integrated <strong _ngcontent-iix-c112="">CI/CD</strong> for teams to develop awesome looking font-ends Frameworks like Angular and React and Single Code Base Frameworks for mobile like <strong _ngcontent-iix-c112="">Flutter</strong>. </li></ul></div><button _ngcontent-iix-c112="" onclick="readMore({dts: \'dots3\', mt: \'more3\', btn: \'rm3\'})" id="rm3" class="no-btn text-danger"></button></div>',
        tags: [
          'Team Leadership',
          'Team Building',
          'Project Management',
          'Servant Leadership',
          'Product Management',
          'System Design',
          'Typescript',
          'Angular',
          'React',
          'Python',
          'Nodejs',
          'Cloud Security',
          'CICD',
          'DevOps',
          'DevSecOps',
        ],
      },
      {
        Link: 'https://ansaries.com',
        imageLink: 'assets/imgs/ansaries.jpeg',
        alt: 'kaiops_logo',
        title: 'Planning Director / Co-Founder',
        subtitle: 'Ansari Security Systems',
        date: '2015 - 2019',
        status: 'Dubai, UAE (Full Time On-Site)',
        short:
          '<div _ngcontent-dnh-c112="" class="de"> An ICT Solutions Provider across a range of industries including Public, Defense, Hospitality, Healthcare, Education, Retail, Royal Offices and other Private Sectors. The company has core experience of design, build and implementation of Current Edge Surveillance Systems, Digital Media Transmission and Control Systems and state-of-the-art Automation Systems. I have had my hands on following major aspects of business: <span _ngcontent-dnh-c112="" id="dots4">...</span>',
        more: '<div _ngcontent-dnh-c112="" id="more4" class=""><ul _ngcontent-dnh-c112=""><li _ngcontent-dnh-c112=""> Created Infrastructure, Security, Audio Visual and Automation Divisions with each one having their own Revenue Streams and which generated a total of 14M AED average yearly revenue. </li><li _ngcontent-dnh-c112=""> Managed multiple Key Accounts and their Projects mainly in Public and Defense Verticals </li><li _ngcontent-dnh-c112=""> Lead and worked closely with the Project Management Teams to develop and deploy professional solutions. </li><li _ngcontent-dnh-c112=""> Formed a formal Contracts Management portfolio to manage long terms contracts. </li><li _ngcontent-dnh-c112=""> Created efficient process for Estimation, Sales Engineering, Specification and Compliance Management. </li><li _ngcontent-dnh-c112=""> Developed an R&amp;D Department to do continuous research and development to always keep our solutions one step ahead of the general market. </li><li _ngcontent-dnh-c112=""> Lead local and remote Engineering Teams for Projects in all over MENA mostly in Saudi Arabia, Qatar, Oman,UAE and multiple countries in Africa. </li></ul></div><button _ngcontent-dnh-c112="" onclick="readMore({dts: \'dots4\', mt: \'more4\', btn: \'rm4\'})" id="rm4" class="no-btn text-danger"></button></div>',
        tags: [
          'Team Leadership',
          'Team Building',
          'Project Management',
          'Servant Leadership',
          'Product Management',
          'System Design',
          'Security',
          'Audio Visual',
          'Automation',
          'CCTV',
          'Access Control',
          'Digital Signage',
          'Digital Media',
          'Digital Media Storage',
          'Embeded Systems Programming',
          'Control Systems Programming',
        ],
      },
      {
        Link: '',
        imageLink: 'assets/imgs/kayos.png',
        alt: 'kaiops_logo',
        title: 'Software Developer',
        subtitle: 'Kayos Works',
        date: '2002 - 2003',
        status: 'Dubai, UAE (Full Time On-Site)',

        short:
          '<div _ngcontent-xvp-c112="" class="de"> KayosWorks is a Consultants owned IT services and Software Development Studio based in New Jersey, USA, with its local office in Islamabad Pakistan. They were working on a distributed solution for major federal agencies. <span _ngcontent-xvp-c112="" id="dots5">...</span>',
        more: '<div _ngcontent-xvp-c112="" id="more5" class=""><ul _ngcontent-xvp-c112=""><li _ngcontent-xvp-c112=""><strong _ngcontent-xvp-c112="">Optimized Query Engine</strong><br _ngcontent-xvp-c112=""><span _ngcontent-xvp-c112="">OQE was an ETL (Extract transform and Loading tool) Developed using Visual C++ (COM/DCOM) for optimization of a health care product named Visual Practice ( www.visualpractice.com ). Visual Practice was originally developed in Visual FoxPro 6.0. The component uses high performance data caching and synchronization mechanism.</span></li><li _ngcontent-xvp-c112=""><strong _ngcontent-xvp-c112="">Visual Practice 2.0 Enterprise Edition</strong><span _ngcontent-xvp-c112="">It was an application with heterogeneous systems architecture; the database is located on a Linux based server which was maintained by PostgreSQL server and the GUI/ front end was developed for Windows platform. The front end of the application was developed in VC#, the middle tier was developed in VC++ .Net and the database is connected through ADO .Net using pgSql (An oledb driver for PostgreSQL).</span></li></ul></div><button _ngcontent-xvp-c112="" onclick="readMore({dts: \'dots5\', mt: \'more5\', btn: \'rm5\'})" id="rm5" class="no-btn text-danger"></button></div>',
        tags: [
          'Visual C++',
          'System Design',
          'Visual Basic',
          'C/C++',
          'Visual Foxpro',
          'COM/DCOM',
          'PostgreSQL',
        ],
      },
    ],
    education: [
      {
        qualification: 'MS Software Engineering',
        university: 'Bahria University Islamabad Pakistan',
      },
      {
        qualification: 'BS Software Engineering',
        university: 'Bahria University Islamabad Pakistan',
      },
    ],
    skills: [
      {
        name: 'Kubernetes',
        score: '97%',
      },
      {
        name: 'JavaScript',
        score: ' 95%',
      },
      {
        name: 'Nodejs',
        score: '95%',
      },
      {
        name: 'MongoDB',
        score: '90%',
      },
      {
        name: 'Angular',
        score: '97%',
      },
      {
        name: 'Python',
        score: '80%',
      },
      {
        name: 'Flutter',
        score: '85%',
      },
      {
        name: 'React',
        score: '85%',
      },
      {
        name: 'Dot Net',
        score: '80%',
      },
      {
        name: 'SQL',
        score: '80%',
      },
      {
        name: 'DevOps',
        score: '95%',
      },
      {
        name: 'DevSecOps',
        score: '85%',
      },
    ],
    aptitude: [
      {
        title: 'Team Building',
        score: '95%',
      },
      {
        title: 'Product Management',
        score: '95%',
      },
      {
        title: 'Servant Leadership',
        score: '100%',
      },
      {
        title: 'System Design',
        score: '93%',
      },
      {
        title: 'Presentation',
        score: '93%',
      },
    ],
  },

  banValues: [
    { class: 'ti-alarm-clock icon-xl', score: '50%', name: 'Development' },
    { class: 'ti-layers-alt icon-xl', score: '10%', name: 'Integration' },
    { class: 'ti-face-smile icon-xl', score: '30%', name: 'Coordination' },
    {
      class: 'ti-heart-broken icon-xl',
      score: '10%',
      name: 'Knowledge Sharing',
    },
  ],

  projects: {
    highlightedTitle: 'Freelance ',
    Title: ' Projects',

    myWork: [
      {
        icon: 'ti-vector text-danger',
        name: 'CMF (Contract Management Framework)',
        spot: 'Nokia Global -- Freelance',
        details:
          '<p _ngcontent-gtt-c110="" class="mh-18x"> Design and Developed a Prototype of Digitization of Contract Management Framework (CMF) which was later developed in-house by Nokia. The product was later recognized by World Commerce and Contracting Association (WCCA) and awarded Nokia Pre-Sales and Contract Management as <strong _ngcontent-gtt-c110="">EMEA Winner of Strategic Direction Award 2020</strong></p>',
      },
      {
        icon: 'ti-bar-chart text-danger',
        name: 'BI Visualizer',
        spot: ' European Stock Exchange -- Freelance',
        details:
          '<p _ngcontent-aty-c110="" class="mh-18x"> Designed and Developed a microservice-based Prototype for BI, Visualization, Analyses, and Manipulation of Big Data sourcing from <strong _ngcontent-aty-c110="">Snowflake</strong>. Tech Stack consisted <b _ngcontent-aty-c110="">Angular</b>, <b _ngcontent-aty-c110="">NestJs</b>, <b _ngcontent-aty-c110="">Kubernetes</b>, <b _ngcontent-aty-c110="">CICD</b>, <b _ngcontent-aty-c110="">Helm</b>, <b _ngcontent-aty-c110="">MongoDB</b>, <b _ngcontent-aty-c110="">D3js</b> and <b _ngcontent-aty-c110="">Snowflakes</b> etc. The product was later developed in-house by the company. </p>',
      },
      {
        icon: 'ti-bar-chart text-danger',
        name: ' European Stock Exchange -- Freelance',
        spot: 'Designed and Developed an ETL to source data from Qualys API into Snowflake for BI',
        details:
          '<p _ngcontent-jkv-c110="" class="mh-18x"> Design and Developed multiple high performance, robust, multi-threaded ETL scripts to pull vulnerabilities and hosts detections data from Qualays Api Server using <b _ngcontent-jkv-c110="">Python</b>, <b _ngcontent-jkv-c110="">NodeJS</b> and <b _ngcontent-jkv-c110="">Matillion</b>. The data was then loaded into <b _ngcontent-jkv-c110="">Snowflake</b> for <b _ngcontent-jkv-c110="">BI</b> and <b _ngcontent-jkv-c110="">Visualization</b>. </p>',
      },
      {
        icon: 'ti-support text-danger',
        name: 'CallusDoctors.com',
        spot: 'Remedial Medical USA -- Freelance',
        details:
          '<p _ngcontent-srx-c110="" class="mh-18x"> Design and Developed a Patient and Practice Management Web Application for a US Based Health Care Institution. The product was built using <strong _ngcontent-srx-c110="">Angular</strong> and <strong _ngcontent-srx-c110="">NodeJS</strong> using state of the art cloud native architecture. </p>',
      },
    ],
  },

  availabilityBanner: {
    position: 'I Am Available For mentions, collaborations, and freelance',
    buttonText: 'Get in touch',
  },

  articles: {
    highlightedTitle: 'Latest',
    title: ' Articles',
    blogLink: 'https://blog.arslanali.io',
    buttonText: 'View All',

    blog: [
      {
        blogLink: 'https://blog.arslanali.io',
        name: 'Arslan Ali',
        imageLink: 'assets/imgs/blog3.png',
        alt: 'blog',
        title:
          'Application Deployment using Gitlab CI/CD on Managed Kubernetes Cluster at GCP',
        Link2:
          'https://faun.pub/application-deployment-using-gitlab-ci-cd-on-managed-kubernetes-cluster-at-gcp-72b59496979c',
        paragraph2:
          "In continuation of my previous article where I've deployed the same mono-repo of microservices application on Docker Swarm instead of Kubernetes, we will repeat most of the steps and I will try to be brief about all the steps except the deployment configuration. So it is highly recommended that you read this article first.",
        likes: '54',
        comments: '2',
        paragraph: {
          firstlines: 'Step by Step guide to deploy a',
          secondlines: ' on GCP using Gitlab CI/CD Pipelines.',
          link: 'https://github.com/dockersamples/example-voting-app',
          bold: true,
          linktext: 'mono-repo of microservices',
        },
      },
      {
        blogLink: 'https://blog.arslanali.io',
        name: 'Arslan Ali',
        imageLink: 'assets/imgs/blog2.jpeg',
        alt: 'blog',
        title: 'Securing the Kubernetes Clusters with AI and ML',
        likes: '',
        comments: '',
        paragraph: {
          firstlines:
            'Kaiops uses AI and ML on top of Rules based anomaly and vulnerability detection, creating a virtually unbreakable line of defense for K8s Clusters.',
          secondlines: ' ',
          link: '',
          bold: false,
          linktext: '',
        },
        Link2:
          'https://blog.arslanali.io/how-to-secure-kubernetes-clusters-with-ai-and-ml',
        paragraph2:
          'More than half of all enterprises consider security as their biggest challenge when publishing their microservice workloads in production. 50% require developers to use validated image only, around 80% want to have a DevSecOps initiative, more than 40% consider DevOps as the role most responsible for Kubernetes security, and most importantly, more than half have delayed application deployment due to security concerns.',
      },
      {
        name: 'Arslan Ali',
        blogLink: 'https://blog.arslanali.io',
        imageLink: 'assets/imgs/blog1.jpeg',
        alt: 'blog',
        likes: '76',
        comments: '',
        title:
          'Microservices based Application Deployment using Gitlab CI/CD on Docker Swarm',
        paragraph: {
          secondlines: '',
          firstlines: ' ',
          link: '',
          bold: false,
          linktext: '',
        },
        paragraph2:
          'Using Docker’s official sample voting app microservices example to implement Gitlab’s CI/CD and deploy them on Docker Swarm will be the agenda. In my next article, I will use the same but deploy it on Kubernetes Cluster. Let’s do this!….',

        Link2:
          'https://faun.pub/microservices-based-application-deployment-using-gitlab-ci-cd-on-docker-swarm-at-digital-ocean-586eefb07294',
      },
    ],
  },
  portfolio: {
    highlightedTitle: 'Opensource',
    title: 'Portfolio (Work in progress 😊 )',
    links: [
      { title: 'New', link: '', data: '.new', class: 'active' },
      { title: 'Security', link: '', data: '.advertising', class: '' },
      { title: 'DevOps', link: '', data: '.branding', class: '' },
      { title: 'Others', link: '', data: '.web', class: '' },
    ],
    itemCard: [
      {
        class: 'item long card',
        imageLink: 'assets/imgs/blog1.jpg',
        Link: 'https://gitlab.com/ansaries',
        header: 'Services Mono Repo Starter',
        body: 'A mono-repo for Multi Language microservices based application skeleton,built with Angular and React Frontends, nginx for serving static assets and proxing apirequests, NestJs for backend Gateway and DB Workers.',
      },
      {
        class: 'item card',
        imageLink: 'assets/imgs/web-1.jpg',
        Link: 'https://gitlab.com/ansaries',
        header: 'Services Mono Repo Starter',
        body: 'A mono-repo for Multi Language microservices based application skeleton,built with Angular and React Frontends, nginx for serving static assets and proxing apirequests, NestJs for backend Gateway and DB Workers.',
      },
    ],
    portfolioItem: [
      {
        class: 'col-md-6 col-lg-4 web new',
        imageLink: 'assets/imgs/web-2.jpg',
        title: ' WEB',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising new',
        imageLink: 'assets/imgs/advertising-2.jpg',
        title: ' ADVERSTISING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 web',
        imageLink: 'assets/imgs/web-4.jpg',
        title: ' WEB',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising',
        imageLink: 'assets/imgs/advertising-1.jpg',
        title: ' advertising',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 web new',
        imageLink: 'assets/imgs/web-3.jpg',
        title: ' WEB',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising new',
        imageLink: 'assets/imgs/advertising-3.jpg',
        title: ' ADVERSITING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 advertising new',
        imageLink: 'assets/imgs/advertising-4.jpg',
        title: ' ADVERSITING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-2.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-3.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-4.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
      {
        class: 'col-md-6 col-lg-4 branding new',
        imageLink: 'assets/imgs/branding-5.jpg',
        title: ' BRANDING',
        subtitle: 'Expedita corporis doloremque velit in tm!',
      },
    ],
  },

  review: [
    {
      class: 'carousel-item active',
      paragragh:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.',
      imageLink: 'https://i.ibb.co/8x9xK4H/team.jpg',
      name: 'Paula Wilson 2',
      detail: 'Media Analyst / SkyNet',
    },
    {
      class: 'carousel-item',
      paragragh:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.',
      imageLink: 'https://i.ibb.co/8x9xK4H/team.jpg',
      name: 'Paula Wilson 3',
      detail: 'Media Analyst / SkyNet',
    },
    {
      class: 'carousel-item',
      paragragh:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.',
      imageLink: 'https://i.ibb.co/8x9xK4H/team.jpg',
      name: 'Paula Wilson 4',
      detail: 'Media Analyst / SkyNet',
    },
    {
      class: 'carousel-item ',
      paragragh:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.',
      imageLink: 'https://i.ibb.co/8x9xK4H/team.jpg',
      name: 'Paula Wilson 1',
      detail: 'Media Analyst / SkyNet',
    },
    {
      class: 'carousel-item',
      paragragh:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.',
      imageLink: 'https://i.ibb.co/8x9xK4H/team.jpg',
      name: 'Paula Wilson 5',
      detail: 'Media Analyst / SkyNet',
    },
    {
      class: 'carousel-item',
      paragragh:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.',
      imageLink: 'https://i.ibb.co/8x9xK4H/team.jpg',
      name: 'Paula Wilson',
      detail: 'Media Analyst / SkyNet',
    },
  ],
  contacts: [
    { title: 'Phone', class: 'ti-mobile icon-md', value: '+1 301 7414 229' },
    {
      title: 'Address',
      class: 'ti-map-alt icon-md',
      value:
        'Planet Earth, The Solar System, Oort Cloud, Local Fluff,Local Bubble, Orion Arm, Milky Way Galaxy, Local Group,Virgo Supercluster, Laniakea Supercluster, Universe',
    },

    { title: 'Email', class: 'ti-envelope icon-md', value: 'iam@arslanali.io' },
  ],
  footervalue: [
    {
      portfolioLink: 'https://gitlab.com/ansaries/personal-portfolio',
      name: 'Arslan Ali',
    },
  ],

  socials: [
    {
      name: 'facebook',
      icon: 'ti-facebook',
      link: 'https://www.facebook.com/arslanaliansari',
    },
    {
      name: 'twitter',
      icon: 'ti-twitter',
      link: 'https://twitter.com/aansaries',
    },
    {
      name: 'youtube',
      icon: 'ti-youtube',
      link: 'https://www.youtube.com/channel/UCd8eSCcPRumYUWA-ca27kUQ',
    },
    {
      name: 'linkedin',
      icon: 'ti-linkedin',
      link: 'https://linkedin.com/in/arslan-ali-ansari',
    },
    { name: 'gitlab', icon: 'ti-github', link: 'https://gitlab.com/ansaries' },
  ],
};
