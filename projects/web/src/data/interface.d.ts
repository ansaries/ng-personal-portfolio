declare module ProfileNamespace {
  export interface Certification {
    certificateLink: string;
    class: string;
    badgeLink: string;
    alt: string;
    title: string;
  }

  export interface MainBanner {
    title: string;
    yourName: string;
    jobPositions: string;
    resumeLink: string;
    buttonText: string;
    portfolioLink: string;
    certification: Certification[];
  }

  export interface LinkText {
    title1: string;
    linkHome: string;
    title2: string;
    linkAbout: string;
    title3: string;
    linkResume: string;
    title4: string;
    linkPortfolio: string;
    title5: string;
    linkBlog: string;
    title6: string;
    linkContact: string;
  }

  export interface Nav {
    linkText: LinkText;
    avatarLink: string;
    avatarTitle: string;
  }

  export interface Summary {
    title: string;
    button: string;
    aboutTitle: string;
    short: string;
    detail: string;
  }

  export interface PersonalInfo {
    title: string;
    heading1: string;
    email: string;
    heading2: string;
    phone: string;
    heading3: string;
    skype: string;
  }

  export interface Description {
    class: string;
    title: string;
    subtitle: string;
  }

  export interface Expertiese {
    title: string;
    description: Description[];
  }

  export interface About {
    summary: Summary;
    personalInfo: PersonalInfo;
    expertiese: Expertiese;
  }

  export interface WorkHistory {
    Link: string;
    imageLink: string;
    alt: string;
    title: string;
    subtitle: string;
    date: string;
    status: string;
    short: string;
    more: string;
    tags: string[];
  }

  export interface Education {
    qualification: string;
    university: string;
  }

  export interface Skill {
    name: string;
    score: string;
  }

  export interface Aptitude {
    title: string;
    score: string;
  }

  export interface Resume {
    mainTitle: string;
    heading1: string;
    heading2: string;
    heading3: string;
    heading4: string;
    workHistory: WorkHistory[];
    education: Education[];
    skills: Skill[];
    aptitude: Aptitude[];
  }

  export interface BanValue {
    class: string;
    score: string;
    name: string;
  }

  export interface MyWork {
    icon: string;
    name: string;
    spot: string;
    details: string;
  }

  export interface Projects {
    highlightedTitle: string;
    Title: string;
    myWork: MyWork[];
  }

  export interface AvailabilityBanner {
    position: string;
    buttonText: string;
  }

  export interface Paragraph {
    firstlines: string;
    secondlines: string;
    link: string;
    bold: boolean;
    linktext: string;
  }

  export interface Blog {
    blogLink: string;
    name: string;
    imageLink: string;
    alt: string;
    title: string;
    Link2: string;
    paragraph2: string;
    likes: string;
    comments: string;
    paragraph: Paragraph;
  }

  export interface Articles {
    highlightedTitle: string;
    title: string;
    blogLink: string;
    buttonText: string;
    blog: Blog[];
  }

  export interface Link {
    title: string;
    link: string;
    data: string;
    class: string;
  }

  export interface ItemCard {
    class: string;
    imageLink: string;
    Link: string;
    header: string;
    body: string;
  }

  export interface PortfolioItem {
    class: string;
    imageLink: string;
    title: string;
    subtitle: string;
  }

  export interface Portfolio {
    highlightedTitle: string;
    title: string;
    links: Link[];
    itemCard: ItemCard[];
    portfolioItem: PortfolioItem[];
  }

  export interface Review {
    class: string;
    paragragh: string;
    imageLink: string;
    name: string;
    detail: string;
  }

  export interface Contact {
    title: string;
    class: string;
    value: string;
  }

  export interface Footervalue {
    portfolioLink: string;
    name: string;
  }

  export interface Social {
    name: string;
    icon: string;
    link: string;
  }

  export interface Persona {
    mainBanner: MainBanner;
    nav: Nav;
    about: About;
    resume: Resume;
    banValues: BanValue[];
    projects: Projects;
    availabilityBanner: AvailabilityBanner;
    articles: Articles;
    portfolio: Portfolio;
    review: Review[];
    contacts: Contact[];
    footervalue: Footervalue[];
    socials: Social[];
  }
}
