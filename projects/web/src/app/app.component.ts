import { Component } from '@angular/core';
// import data from '../data/data.json';
// import { sumanData } from '../data/suman';
import { arslanData } from '../data/arslan';
// import { asifData } from '../data/asif';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'web';
  profileData: any;

  constructor() {}
  ngOnInit() {
    // this.profileData = data  //access data from json
    this.profileData = arslanData;
    console.log(this.profileData);
  }
}
