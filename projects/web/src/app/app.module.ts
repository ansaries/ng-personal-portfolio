import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SocialIconsModule } from './components/social-icons/social-icons.module';
import { CertificatesModule } from './components/certificates/certificates.module';
import { ContactModule } from './components/contact/contact.module';
import { FooterModule } from './components/footer/footer.module';
import { FormModule } from './components/form/form.module';
import { PortfolioModule } from './components/portfolio/portfolio.module';
import { BlogModule } from './components/blog/blog.module';
import { ServicesModule } from './components/services/services.module';
import { BannerModule } from './components/banner/banner.module';
import { ResumeModule } from './components/resume/resume.module';
import { MiniModule } from './components/mini/mini.module';
import { AboutModule } from './components/about/about.module';
import { ResWorkHistoryModule } from './components/res-work-history/res-work-history.module';
import { EducationModule } from './components/education/education.module';
import { HoursModule } from './components/hours/hours.module';
import { FeedbackModule } from './components/feedback/feedback.module';
// import { ResWorkHistoryComponent } from './components/res-work-history/res-work-history.component';

@NgModule({
  declarations: [
    AppComponent,
    // ResWorkHistoryComponent,
  ],
  providers: [],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    SocialIconsModule,
    CertificatesModule,
    ContactModule,
    FooterModule,
    FormModule,
    PortfolioModule,

    BlogModule,
    ServicesModule,
    BannerModule,
    ResumeModule,
    MiniModule,
    AboutModule,
    ResWorkHistoryModule,
    EducationModule,
    HoursModule,
    FeedbackModule,
  ],
})
export class AppModule {}
