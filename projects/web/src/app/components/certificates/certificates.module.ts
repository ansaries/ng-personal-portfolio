import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificatesComponent } from './certificates.component';



@NgModule({
  declarations: [
    CertificatesComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CertificatesComponent //mandatory to export bcz it is imported in app.module by its module
  ]
})
export class CertificatesModule { }
