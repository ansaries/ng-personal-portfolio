import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MiniComponent } from './mini.component';



@NgModule({
  declarations: [
    MiniComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    MiniComponent
  ]
})
export class MiniModule { }
