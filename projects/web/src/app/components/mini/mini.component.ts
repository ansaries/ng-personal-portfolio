import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mini',
  templateUrl: './mini.component.html',
  styleUrls: ['./mini.component.scss'],
})
export class MiniComponent implements OnInit {
  @Input() values: any;

  ngOnInit(): void {
    console.log(this.values);
  }
}
