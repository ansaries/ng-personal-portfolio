import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResumeComponent } from './resume.component';
import { ResWorkHistoryModule } from "../res-work-history/res-work-history.module";
import { EducationModule } from "../education/education.module";
import { HoursModule } from "../hours/hours.module";
import { AptitudeModule } from "../aptitude/aptitude.module";



@NgModule({
    declarations: [
        ResumeComponent
    ],
    exports: [
        ResumeComponent
    ],
    imports: [
        CommonModule,
        ResWorkHistoryModule,
        EducationModule,
        HoursModule,
        AptitudeModule
    ]
})
export class ResumeModule { }
