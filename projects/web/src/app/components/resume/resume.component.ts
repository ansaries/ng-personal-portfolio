import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss'],
})
export class ResumeComponent implements OnInit {
  @Input() aptitude: any[] = [];
  @Input() education: any[] = [];
  @Input() hours: any[] = [];
  @Input() workData: any[] = [];
  @Input() resume: any;

  ngOnInit() {
    console.log(this.aptitude);
  }
}
