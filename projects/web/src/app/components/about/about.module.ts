import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about.component';
import { SocialIconsModule } from "../social-icons/social-icons.module";



@NgModule({
    declarations: [
        AboutComponent
    ],
    exports: [
        AboutComponent
    ],
    imports: [
        CommonModule,
        SocialIconsModule
    ]
})
export class AboutModule { }
