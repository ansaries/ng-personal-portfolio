import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  @Input() social: any[] = [];
  @Input() textColor = 'light';
  @Input() summary: any | undefined;
  @Input() Link = '';
  @Input() personalInfo: any | undefined;
  @Input() experties: any | undefined;
  @Input() aboutTitle: any;

  showMoreContent = false;
  showMore() {
    this.showMoreContent = !this.showMoreContent;
  }

  ngOnInit(): void {
    console.log(this.summary.detail);
  }
}
