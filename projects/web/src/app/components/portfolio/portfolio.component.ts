import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss'],
})
export class PortfolioComponent {
  // @Input() itemCard: any[] = [];
  @Input() portfolio: any;

  // ngOnChanges(changes:SimpleChanges){
  //   console.log(changes, ' portfolio 123');

  // }
}
