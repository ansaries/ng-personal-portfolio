import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { FormModule } from "../form/form.module";
import { SocialIconsModule } from "../social-icons/social-icons.module";

@NgModule({
    declarations: [
        ContactComponent
    ],
    exports: [
        ContactComponent
    ],
    imports: [
        CommonModule,
        FormModule,
        SocialIconsModule
    ]
})
export class ContactModule { }
