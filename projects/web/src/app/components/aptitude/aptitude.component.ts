import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-aptitude',
  templateUrl: './aptitude.component.html',
  styleUrls: ['./aptitude.component.scss'],
})
export class AptitudeComponent implements OnInit, OnChanges {
  @Input() aptitude: any | undefined;

  ngOnChanges(changes: SimpleChanges) {
    console.log('Aptitude', changes['aptitude'].currentValue);
  }
  ngOnInit() {
    console.log(this.aptitude);
  }
}
