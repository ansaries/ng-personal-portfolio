import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AptitudeComponent } from './aptitude.component';



@NgModule({
  declarations: [
    AptitudeComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    AptitudeComponent
  ]
})
export class AptitudeModule { }
