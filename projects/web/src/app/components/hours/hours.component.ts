import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hours',
  templateUrl: './hours.component.html',
  styleUrls: ['./hours.component.scss'],
})
export class HoursComponent implements OnInit {
  @Input() hours: any;

  ngOnInit() {
    console.log(this.hours);
  }
}
