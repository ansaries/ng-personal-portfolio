import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HoursComponent } from './hours.component';



@NgModule({
  declarations: [
    HoursComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    HoursComponent
    
  ]
})
export class HoursModule { }
