import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-social-icons',
  templateUrl: './social-icons.component.html',
  styleUrls: ['./social-icons.component.scss']
})
export class SocialIconsComponent {
  //giving inputs from app-component (parent component)
  @Input() social: any[] = []; 
  @Input() textColor = '';
}
