import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResWorkHistoryComponent } from './res-work-history.component';



@NgModule({
  declarations: [    
    ResWorkHistoryComponent
  ],
  imports: [
    CommonModule,
  ],
  exports:[
    ResWorkHistoryComponent
  ]
})
export class ResWorkHistoryModule { }
