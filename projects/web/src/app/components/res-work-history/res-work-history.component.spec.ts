import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResWorkHistoryComponent } from './res-work-history.component';

describe('ResWorkHistoryComponent', () => {
  let component: ResWorkHistoryComponent;
  let fixture: ComponentFixture<ResWorkHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResWorkHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResWorkHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
