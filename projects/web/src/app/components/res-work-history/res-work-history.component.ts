import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-res-work-history',
  templateUrl: './res-work-history.component.html',
  styleUrls: ['./res-work-history.component.scss'],
})
export class ResWorkHistoryComponent implements OnInit {
  @Input() workData: any[] = [];
  @Input() resume: any;

  showMoreContent = false;

  showMore() {
    this.showMoreContent = !this.showMoreContent;
  }

  ngOnInit(): void {
    console.log(this.resume.workHistory);
  }
}
