/*!
=========================================================
* JohnDoe Landing page
=========================================================

* Copyright: 2019 DevCRUD (https://devcrud.com)
* Licensed: (https://devcrud.com/licenses)
* Coded by www.devcrud.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// smooth scroll
$(document).ready(function () {
  $(".navbar .nav-link").on("click", function (event) {
    if (this.hash !== "") {
      event.preventDefault();

      var hash = this.hash;

      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top,
        },
        700,
        function () {
          window.location.hash = hash;
        }
      );
    }
  });
});

//swipper........
// $(document).ready(function () {
//   $("#testimonial-slider").owlCarousel({
//     items: 1,
//     itemsDesktop: [1000, 1],
//     itemsDesktopSmall: [979, 1],
//     itemsTablet: [768, 1],
//     pagination: false,
//     navigation: true,
//     navigationText: ["", ""],
//     autoPlay: true,
//   });
// });
// var swiper = new Swiper(".mySwiper", {
//   slidesPreView: 1,
//   spaceBetween: 30,
//   loop: true,
//   pagination: {
//     el: ".swiper-pagination",
//     clickable: true,
//   },
//   navigation: {
//     nextEl: ".swiper-button-next",
//     prevEl: ".swiper-button-prev",
//   },
// });

$(window).on("load", function () {
  // Enable Tooltips
  // $(function () {
  //   $('[data-toggle="tooltip"]').tooltip()
  // })

  // protfolio filters
  var t = $(".portfolio-container");
  t.isotope({
    filter: ".new",
    animationOptions: {
      duration: 750,
      easing: "ease-in",
      queue: !1,
    },
  });
  $(".filters a").click(function () {
    $(".filters .active").removeClass("active"), $(this).addClass("active");
    var i = $(this).attr("data-filter");
    return (
      t.isotope({
        filter: i,
        animationOptions: {
          duration: 750,
          easing: "linear",
          queue: !1,
        },
      }),
      !1
    );
  });

  // $('[data-toggle="popover"]').popover();
});

function readMore({ dts, mt, btn }) {
  var dots = document.getElementById(dts);
  var moreText = document.getElementById(mt);
  var btnText = document.getElementById(btn);
  if (dots.style.display === "none") {
    moreText.classList.replace("large-size", "small-size");
    setTimeout(() => {
      btnText.innerHTML = "Read more";
      dots.style.display = "inline";
    }, 200);
  } else {
    dots.style.display = "none";
    moreText.classList.replace("small-size", "large-size");
    setTimeout(() => {
      btnText.innerHTML = "Read less";
    }, 200);
  }
}

function readMoreAll() {
  Array(5)
    .fill(0)
    .forEach((_, i) => {
      readMore({ dts: `dots${i + 1}`, mt: `more${i + 1}`, btn: `rm${i + 1}` });
    });
  var dots = document.getElementById("dots1");
  var icon = document.getElementById("show-all-icn");

  if (dots.style.display === "none") {
    icon.classList.add("ti-menu-alt");
    icon.classList.remove("ti-menu");
  } else {
    icon.classList.remove("ti-menu-alt");
    icon.classList.add("ti-menu");
  }
}

// google maps
function initMap() {
  // Styles a map in night mode.
  setTimeout(() => {
    var map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: 39.276731, lng: -76.9009934 },
      zoom: 12,
      scrollwheel: false,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      styles: [
        { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
        { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
        { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
        {
          featureType: "administrative.locality",
          elementType: "labels.text.fill",
          stylers: [{ color: "#d59563" }],
        },
        {
          featureType: "poi",
          elementType: "labels.text.fill",
          stylers: [{ color: "#d59563" }],
        },
        {
          featureType: "poi.park",
          elementType: "geometry",
          stylers: [{ color: "#263c3f" }],
        },
        {
          featureType: "poi.park",
          elementType: "labels.text.fill",
          stylers: [{ color: "#6b9a76" }],
        },
        {
          featureType: "road",
          elementType: "geometry",
          stylers: [{ color: "#38414e" }],
        },
        {
          featureType: "road",
          elementType: "geometry.stroke",
          stylers: [{ color: "#212a37" }],
        },
        {
          featureType: "road",
          elementType: "labels.text.fill",
          stylers: [{ color: "#9ca5b3" }],
        },
        {
          featureType: "road.highway",
          elementType: "geometry",
          stylers: [{ color: "#746855" }],
        },
        {
          featureType: "road.highway",
          elementType: "geometry.stroke",
          stylers: [{ color: "#1f2835" }],
        },
        {
          featureType: "road.highway",
          elementType: "labels.text.fill",
          stylers: [{ color: "#f3d19c" }],
        },
        {
          featureType: "transit",
          elementType: "geometry",
          stylers: [{ color: "#2f3948" }],
        },
        {
          featureType: "transit.station",
          elementType: "labels.text.fill",
          stylers: [{ color: "#d59563" }],
        },
        {
          featureType: "water",
          elementType: "geometry",
          stylers: [{ color: "#17263c" }],
        },
        {
          featureType: "water",
          elementType: "labels.text.fill",
          stylers: [{ color: "#515c6d" }],
        },
        {
          featureType: "water",
          elementType: "labels.text.stroke",
          stylers: [{ color: "#17263c" }],
        },
      ],
    });
  }, 1000);
}
